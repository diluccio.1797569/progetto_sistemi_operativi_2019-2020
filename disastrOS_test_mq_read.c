#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include "disastrOS.h"

void test_mqRead_runnerFunc(void *argsPtr)
{
    int res;
    int exp;
    char buf[256];
    unsigned priority;
    unsigned passed_tests = 0;
    unsigned tests = 0;

    disastrOS_mqCreate("/mq");
    int mqfd = disastrOS_mqOpen("/mq", DSOS_READ | DSOS_WRITE);
    srand(time(NULL));
    disastrOS_mqWrite(mqfd, "OIGGASSEM", 9, rand() % 10);
    disastrOS_mqWrite(mqfd, "MESSAGGIO", 9, rand() % 10);
    disastrOS_printStatus();

    ++tests;
    exp = 9;
    buf[0] = 0;
    priority = 20;
    printf("RUNNER mqRead(mqfd, buf, 256, &priority)\n");
    res = disastrOS_mqRead(mqfd, buf, 256, &priority);
    printf("RUNNER Read: '%s', priority %u\n", buf, priority);
    disastrOS_printStatus();
    passed_tests += (res == exp ? 1 : 0);
    printf("RUNNER Test %u: result = %d, expected = %d -> %s!\n\n", tests, res, exp,
        res == exp ? "PASSED" : "FAILED");
    
    ++tests;
    exp = 9;
    buf[0] = 0;
    priority = 20;
    printf("RUNNER mqRead(mqfd, buf, 256, &priority)\n");
    res = disastrOS_mqRead(mqfd, buf, 256, &priority);
    printf("RUNNER Read: '%s', priority %u\n", buf, priority);
    disastrOS_printStatus();
    passed_tests += (res == exp ? 1 : 0);
    printf("RUNNER Test %u: result = %d, expected = %d -> %s!\n\n", tests, res, exp,
        res == exp ? "PASSED" : "FAILED");
    
    ++tests;
    exp = -1;
    buf[0] = 0;
    priority = 20;
    printf("RUNNER mqRead(-1, buf, 256, &priority)\n");
    res = disastrOS_mqRead(-1, buf, 256, &priority);
    printf("RUNNER Read: '%s', priority %u\n", buf, priority);
    disastrOS_printStatus();
    passed_tests += (res == exp ? 1 : 0);
    printf("RUNNER Test %u: result = %d, expected = %d -> %s!\n\n", tests, res, exp,
        res == exp ? "PASSED" : "FAILED");

    ++tests;
    exp = -1;
    buf[0] = 0;
    priority = 20;
    printf("RUNNER mqRead(mqfd + 1, buf, 256, &priority)\n");
    res = disastrOS_mqRead(mqfd + 1, buf, 256, &priority);
    printf("RUNNER Read: '%s', priority %u\n", buf, priority);
    disastrOS_printStatus();
    passed_tests += (res == exp ? 1 : 0);
    printf("RUNNER Test %u: result = %d, expected = %d -> %s!\n\n", tests, res, exp,
        res == exp ? "PASSED" : "FAILED");

    ++tests;
    exp = 0;
    buf[0] = 0;
    priority = 20;
    printf("RUNNER mqRead(mqfd, NULL, 256, &priority)\n");
    res = disastrOS_mqRead(mqfd, NULL, 256, &priority);
    printf("RUNNER Read: '%s', priority %u\n", buf, priority);
    disastrOS_printStatus();
    passed_tests += (res == exp ? 1 : 0);
    printf("RUNNER Test %u: result = %d, expected = %d -> %s!\n\n", tests, res, exp,
        res == exp ? "PASSED" : "FAILED");
    
    ++tests;
    exp = 0;
    buf[0] = 0;
    priority = 20;
    printf("RUNNER mqRead(mqfd, buf, 0, &priority)\n");
    res = disastrOS_mqRead(mqfd, buf, 0, &priority);
    printf("RUNNER Read: '%s', priority %u\n", buf, priority);
    disastrOS_printStatus();
    passed_tests += (res == exp ? 1 : 0);
    printf("RUNNER Test %u: result = %d, expected = %d -> %s!\n\n", tests, res, exp,
        res == exp ? "PASSED" : "FAILED");

    disastrOS_mqWrite(mqfd, "MESSAGGIO", 9, rand() % 10);
    disastrOS_printStatus();
    
    ++tests;
    exp = -1;
    buf[0] = 0;
    priority = 20;
    printf("RUNNER mqRead(mqfd, buf, 1, &priority)\n");
    res = disastrOS_mqRead(mqfd, buf, 1, &priority);
    printf("RUNNER Read: '%s', priority %u\n", buf, priority);
    disastrOS_printStatus();
    passed_tests += (res == exp ? 1 : 0);
    printf("RUNNER Test %u: result = %d, expected = %d -> %s!\n\n", tests, res, exp,
        res == exp ? "PASSED" : "FAILED");

    disastrOS_mqClose(mqfd);
    mqfd = disastrOS_mqOpen("/mq", DSOS_WRITE);
    disastrOS_printStatus();

    ++tests;
    exp = -1;
    buf[0] = 0;
    priority = 20;
    printf("RUNNER mqRead(mqfd, buf, 256, &priority)\n");
    res = disastrOS_mqRead(mqfd, buf, 256, &priority);
    printf("RUNNER Read: '%s', priority %u\n", buf, priority);
    disastrOS_printStatus();
    passed_tests += (res == exp ? 1 : 0);
    printf("RUNNER Test %u: result = %d, expected = %d -> %s!\n\n", tests, res, exp,
        res == exp ? "PASSED" : "FAILED"); 

    disastrOS_mqClose(mqfd);
    disastrOS_mqUnlink("/mq");

    printf("\nRUNNER Total: %u/%u tests passed!\n\n", passed_tests, tests);

    printf("RUNNER exit(0)\n");
    disastrOS_exit(0);
}

void test_mqRead(void *argsPtr)
{
    printf("INIT spawn(test_mqRead_runnerFunc, NULL)\n");
    disastrOS_spawn(test_mqRead_runnerFunc, NULL);
    disastrOS_printStatus();

    printf("INIT wait(0, NULL)\n");
    disastrOS_wait(0, NULL);
    disastrOS_printStatus();

    printf("INIT shutdown()\n");
    disastrOS_shutdown();
}