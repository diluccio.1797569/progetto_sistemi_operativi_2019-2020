#pragma once

#include <string.h>
#include <stdio.h>
#include <assert.h>
#include "pool_allocator.h"
#include "disastrOS_resource.h"
#include "disastrOS_descriptor.h"

/**
 * @brief Initializes all data structures needed for strings handling.
 */
void String_init();

/**
 * @brief Allocates a new string with length equal to 0.
 * @returns a pointer to the just allocated string or NULL if an error occours. 
 */
char * String_new();

/**
 * @brief Deallocates a string. The string MUST be allocated with String_new().
 * @param str pointer to the string to deallocate.
 * @returns 0 if the string is deallocated or -1 if an error occurs.
 */
int String_delete(char *str);

/**
 * @brief Data structure that models a message.
 */
typedef struct Message
{
    ListItem _listItem_;
    char *text;
    unsigned len;
} Message;

/**
 * @brief Initializes all data structures needed for messages handling.
 */
void Message_init();

/**
 * @brief Allocotes a new message.
 * @param text pointer to the string which is the text of the message to allocate.
 * @param len length of the text of the message to allocate.
 * @returns a pointer the the just allocated message or NULL if an error occours. 
 */
Message * Message_new(const char *text, unsigned len);

/**
 * @brief Deallocates a message. The message MUST be allocated with Message_new().
 * @param msgPtr pointer to the message to deallocate.
 * @returns 0 if the message is deallocated or -1 if an error occurs.
 */
int Message_delete(Message *msgPtr);

/**
 * @brief Prints a message on the terminal.
 * @param msgPtr pointer to the message to print.
 */
void Message_print(const Message *msgPtr);

/**
 * @brief Data structure that models a list of messages.
 */
typedef ListHead MessageList;

/**
 * @brief Prints a list of messages on the terminal.
 * @param listPtr pointer to the list to print.
 */
void MessageList_print(MessageList *listPtr);

/**
 * @brief Data structure that models a message subqueue with a specified priority.
 */
typedef struct MsgPrioritySubqueue
{
    ListItem _listItem_;
    MessageList messages;
    unsigned priority;
} MsgPrioritySubqueue;

/**
 * @brief Initializes all data structures needed for message subqueues handling.
 */
void MsgPrioritySubqueue_init();

/**
 * @brief Allocates a new message subqueue with a given priority.
 * @param priority priority of the message subqueue to allocate. It MUST be between 0 and
 * MAX_NUM_PRIORITIES - 1.
 * @returns a pointer to the just allocated message subqueue or NULL if an error occurs.
 */
MsgPrioritySubqueue * MsgPrioritySubqueue_new(unsigned priority);

/**
 * @brief Deallocates a message subqueue.
 * The message subqueue MUST be allocated with MsgPrioritySubqueue_new().
 * If there are any remaining messages in the subqueue, they also will be deallocated.
 * @param msqPtr pointer to the message subqueue to deallocate.
 * @returns 0 if the message subqueue is deallocated or -1 if an error occurs.
 */
int MsgPrioritySubqueue_delete(MsgPrioritySubqueue *msqPtr);

/**
 * @brief Prints all the messages in a message subqueue on the terminal.
 * @param msqPtr pointer to the message subqueue whose message have to be printed.
 */
void MsgPrioritySubqueue_print(const MsgPrioritySubqueue *msqPtr);

/**
 * @brief Data structure that models a message queue.
 */
typedef struct MsgQueue
{
    Resource _resource_;
    MsgPrioritySubqueue *subqueuesPtrs[MAX_NUM_PRIORITIES];
    PCB *ownerPCBPtr;
    unsigned unlinkCalled;
    unsigned numMessages;
} MsgQueue;

/**
 * @brief Initializes all data structures needed for message queues handling.
 */
void MsgQueue_init();

/**
 * Allocates a new message queue which will be owned by the running process.
 * @param name pointer to a string that is the name of the message queue to allocate.
 * @param rid rid the message queue to allocate. @see Resource.
 * @param ownerPCBPtr pointer to the owner process PCB which is the running process.
 * @returns a pointer to the just allocated message queue or NULL if an error occurs.
 */
MsgQueue * MsgQueue_new(const char *name, int rid, PCB *ownerPCBPtr);

/**
 * @brief Deallocates a message queue owned by the running process.
 * The message queue MUST be allocated with MsgQueue_new().
 * @param mqPtr pointer to the message queue to deallocate.
 * @returns 0 if the message queue is deallocated or -1 if an error occurs.
 */
int MsgQueue_delete(MsgQueue *mqPtr);

/**
 * @brief Prints a message queue on the terminal.
 * @param mqPtr pointer to the message queue to print.
 */
void MsgQueue_print(const MsgQueue *mqPtr);

/**
 * @brief Data structure that models a message queue reference.
 */
typedef struct MsgQueueRef
{
    ListItem _listItem_;
    MsgQueue *msgQueuePtr;
} MsgQueueRef;

/**
 * @brief Initializes all data structures needed for message queue references handling.
 */
void MsgQueueRef_init();

/**
 * Allocates a new message queue reference.
 * @param msgQueuePtr a pointer to the message queue to reference.
 * @returns a pointer to the just allocated message queue reference or NULL if an error occurs.
 */
MsgQueueRef * MsgQueueRef_new(MsgQueue *mqPtr);

/**
 * @brief Deallocates a message queue reference. The message reference queue MUST be allocated
 * with MsgQueueRef_new().
 * @param msgQueueRefPtr pointer to the message queue reference to deallocate.
 * @returns 0 if the message queue reference is deallocated or -1 if an error occurs.
 */
int MsgQueueRef_delete(MsgQueueRef *mqRefPtr);

/**
 * @brief Prints the message queue referenced by a message queue reference on the terminal.
 * @param msgQueueRefPtr pointer to the message queue reference whose referencing message
 * queue has to be printed.
 */
void MsgQueueRef_print(const MsgQueueRef *mqRefPtr);

/**
 * @brief Data structures that models a list of message queues references.
 */
typedef ListHead MsgQueueRefList;

/**
 * @brief Traverses a list of message queues references finding the first message queue
 * reference that references a message queue with a given name.
 * @param listPtr pointer to the list of message queue references to traverse.
 * @param name pointer to a string that is the name of the referenced message queue to find.
 * @returns a pointer to the found message queue reference or NULL if an error occours or such
 * message queue isn't referenced by any message queue reference in the list.
 */
MsgQueueRef * MsgQueueRefList_findByName(MsgQueueRefList *listPtr, const char *name);

/**
 * @brief Traverses a list of message queues references finding the first message queue
 * reference that references a message queue with a given rid. @see Resource.
 * MUST BE IMPLEMENTED.
 * @param listPtr pointer to the list of message queue references to traverse.
 * @param rid rid of the referenced message queue to find.
 * @returns a pointer to the found message queue reference or NULL if an error occours or such
 * message queue isn't referenced by any message queue reference in the list.
 */
MsgQueueRef * MsgQueueRefList_findByRid(MsgQueueRefList *listPtr, int rid);

/**
 * @brief Prints a list of message queues referenced by a list of message queues references.
 * @param listPtr pointer to a list of message queues references which is referencing a list
 * of message queues.
 */
void MsgQueueRefList_print(MsgQueueRefList *listPtr);