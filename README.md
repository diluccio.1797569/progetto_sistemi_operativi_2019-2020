# Message Queues in disastrOS
This is an implementation of non-persistent and non-blocking message queues in disastrOS (if the message queue has message to deliver and it is destroyed, all the messages are lost; if the message queue is empty, read operation returns and doesn't wait for a message to be written and if the message queue is full, write operation returns and doesn't wait for a message to be read). Since we are working on Linux, I tried to write an implementation that is similar to the POSIX one.

WARNING: if the messages to write are long or if there are too many writing operations, you could see bad printings and bad feedbacks on the terminal, but all the data structures should be consistent and OK :).

This report document will be updated every time a new functionality is developed.

### New or modified data structures and functions
This is only a summary. All the new or modified stuff is documented in the source code.

- **New system calls**:

    -
        ```c
        int disastrOS_mqCreate(const char *name)
        ```
        allocates and creates a new message queue. See [disastrOS.h](disastrOS.h) for more infos.
    -
        ```c
        int disastrOS_mqOpen(const char *name, int flags)
        ```
        opens a message queue created with ```disastrOS_mqCreate()```. See [disastrOS.h](disastrOS.h) for more infos.
        
    - 
        ```c
        int disastrOS_mqClose(int mqfd)
        ```
        closes a message queue opened with ```disastrOS_mqOpen()```. See [disastrOS.h](disastrOS.h) for more infos.
        
    -
        ```c
        int disastrOS_mqUnlink(const char *name)
        ```
        deallocates and destroys a message queue creted with ```disastrOS_mqCreate()```. See [disastrOS.h](disastrOS.h) for more infos.
        
    -
        ```c
        int disastrOS_mqRead(int mqfd, char *text, unsigned len, unsigned *priorityPtr)
        ```
        reads the message from the head of a message queue opened with ```disastrOS_mqOpen()```. See [disastrOS.h](disastrOS.h) for more infos.
        
    - 
        ```c
        int disastrOS_mqWrite(int mqfd, const char *text, unsigned len, unsigned priority)
        ```
        writes a message in the tail of a message queue opened with ```disastrOS_mqOpen()```. See [disastrOS.h](disastrOS.h) for more infos.
    
- **Modified system calls**:

    -
        ```c
        void disastrOS_start(void (*f)(void*), void* f_args, char* logfile)
        ```
        added strings, messages, message subqueues, message queues and message queues references memory management, system call table setted with new system call, added a list of message queues. See [disastrOS.c](disastrOS.c) for more infos.
    
    -
        ```c
        int disastrOS_openResource(const char *namePtr, int resource_id, int type, int mode)
        ```
        added the name parameter. See [disastrOS.h](disastrOS.h) for more infos.
    
    -
        ```c
        void disastrOS_printStatus()
        ```
        added message queues printing on the terminal. See [disastrOS.c](disastrOS.c) for more infos.
        
- **New data structures**:

    -
        ```c
        struct Message
        ```
        models a message. See [disastrOS_msg_queue.h](disastrOS_msg_queue.h) for more infos.
        
    - 
        ```c
        struct MessageList
        ```
        models a list of messages. See [disastrOS_msg_queue.h](disastrOS_msg_queue.h) for more infos.
    
    -
        ```c
        struct MsgPrioritySubqueue
        ```
        models a message subqueue with a specified priority. See [disastrOS_msg_queue.h](disastrOS_msg_queue.h) for more infos.

    -
        ```c
        struct MsgQueue
        ```
        models a message queue. See [disastrOS_msg_queue.h](disastrOS_msg_queue.h) for more infos.
        
    -
        ```c
        struct MsgQueueRef
        ```
        models a message queue reference. See [disastrOS_msg_queue.h](disastrOS_msg_queue.h) for more infos.
        
    -
        ```c
        struct MsgQueueRefList
        ```
        models a list of message queues references. See [disastrOS_msg_queue.h](disastrOS_msg_queue.h) for more infos.

- **Modified data structures**:

    -
        ```c
        struct Descriptor
        ```
        added read, write and execute permissions. See [disastrOS_descriptor.h](disastrOS_descriptor.h) for more infos.
        
    -
        ```c
        struct Resource
        ```
        added name. See [disastrOS_resource.h](disastrOS_resource.h) for more infos.
        
- **New functions**:

    -
        ```c
        void internal_mqCreate()
        ```
        implements ```disastrOS_mqCreate()``` system call. See [disastrOS_syscalls.h](disastrOS_syscalls.h) for more infos.
        
    -
        ```c
        void internal_mqOpen()
        ```
        implements ```disastrOS_mqOpen()``` system call. See [disastrOS_syscalls.h](disastrOS_syscalls.h) for more infos.
        
    -
        ```c
        void internal_mqClose()
        ```
        implements ```disastrOS_mqClose()``` system call. See [disastrOS_syscalls.h](disastrOS_syscalls.h) for more infos.
        
    -
        ```c
        void internal_mqUnlink()
        ```
        implements ```disastrOS_mqUnlink()``` system call. See [disastrOS_syscalls.h](disastrOS_syscalls.h) for more infos.
        
    -
        ```c
        void internal_mqRead()
        ```
        implements ```disastrOS_mqRead()``` system call. See [disastrOS_syscalls.h](disastrOS_syscalls.h) for more infos.
        
    -
        ```c
        void internal_mqWrite()
        ```
        implements ```disastrOS_mqWrite()``` system call. See [disastrOS_syscalls.h](disastrOS_syscalls.h) for more infos.
        
    -
        ```c
        void String_init()
        ```
        initializes all data structures needed for strings handling. See [disastrOS_msg_queue.h](disastrOS_msg_queue.h) for more infos.
        
    -
        ```c
        char * String_new()
        ```
        allocates a new string. See [disastrOS_msg_queue.h](disastrOS_msg_queue.h) for more infos.
        
    -
        ```c
        int String_delete(char *str)
        ```
        deallocates a string allocated with ```String_new()```. See [disastrOS_msg_queue.h](disastrOS_msg_queue.h) for more infos.
        
    -
        ```c
        void Message_init()
        ```
        initializes all data structures needed for messages handling. See [disastrOS_msg_queue.h](disastrOS_msg_queue.h) for more infos.
        
    -
        ```c
        Message * Message_new(char *text, unsigned len)
        ```
        allocates a new message. See [disastrOS_msg_queue.h](disastrOS_msg_queue.h) for more infos.
        
    -
        ```c
        int Message_delete(Message *msgPtr)
        ```
        deallocates a message allocated with ```Message_new()```. See [disastrOS_msg_queue.h](disastrOS_msg_queue.h) for more infos.
        
    -
        ```c
        void Message_print(Message *msgPtr)
        ```
        prints a message on the terminal. See [disastrOS_msg_queue.h](disastrOS_msg_queue.h) for more infos.
    
    -
        ```c
        void MessageList_print(MessageList *listPtr)
        ```
        prints a list of messages on the terminal. See [disastrOS_msg_queue.h](disastrOS_msg_queue.h) for more infos.
    
    -
        ```c
        void MsgPrioritySubqueue_init()
        ```
        initializes all data structures needed for message subqueues handling. See [disastrOS_msg_queue.h](disastrOS_msg_queue.h) for more infos.
        
    -
        ```c
        MsgPrioritySubqueue * MsgPrioritySubqueue_new(unsigned priority)
        ```
        allocates a new message subqueue with a given priority. See [disastrOS_msg_queue.h](disastrOS_msg_queue.h) for more infos.
        
    -
        ```c
        int MsgPrioritySubqueue_delete(MsgPrioritySubqueue *msqPtr)
        ```
        deallocates a message subqueue allocated with ```MsgPrioritySubqueue_new()```. See [disastrOS_msg_queue.h](disastrOS_msg_queue.h) for more infos.
        
    -
        ```c
        void MsgPrioritySubqueue_print(const MsgPrioritySubqueue *msqPtr)
        ``` 
        prints all the messages in a message subqueue on the terminal. See [disastrOS_msg_queue.h](disastrOS_msg_queue.h) for more infos.

    -
        ```c
        void MsgQueue_init()
        ```
        initializes all data structures needed for message queues handling. See [disastrOS_msg_queue.h](disastrOS_msg_queue.h) for more infos.
        
    -
        ```c
        MsgQueue * MsgQueue_new(char *name, int rid, PCB *ownerPCBPtr)
        ```
        allocates a new message queue. See [disastrOS_msg_queue.h](disastrOS_msg_queue.h) for more infos.
        
    -
        ```c
        int MsgQueue_delete(MsgQueue *mqPtr)
        ```
        deallocates a message queue allocated with ```MsgQueue_new()```. See [disastrOS_msg_queue.h](disastrOS_msg_queue.h) for more infos.
        
    -
        ```c
        void MsgQueue_print(MsgQueue *mqPtr)
        ``` 
        prints a message queue on the terminal. See [disastrOS_msg_queue.h](disastrOS_msg_queue.h) for more infos.
        
    -
        ```c
        void MsgQueueRef_init()
        ```
        initializes all data structures needed for message queues references handling. See [disastrOS_msg_queue.h](disastrOS_msg_queue.h) for more infos.
        
    -
        ```c
        MsgQueueRef * MsgQueueRef_new(MsgQueue *mqPtr)
        ```
        allocates a new message queue reference. See [disastrOS_msg_queue.h](disastrOS_msg_queue.h) for more infos.
        
    -
        ```c
        int MsgQueueRef_delete(MsgQueueRef *mqRefPtr)
        ```
        deallocates a message queue reference allocated with ```MsgQueueRef_new()```. See [disastrOS_msg_queue.h](disastrOS_msg_queue.h) for more infos.
        
    -
        ```c
        void MsgQueueRef_print(MsgQueueRef *mqRefPtr)
        ```
        prints the referenced message queue by a message queue reference on the terminal. See [disastrOS_msg_queue.h](disastrOS_msg_queue.h) for more infos.
        
    -
        ```c
        MsgQueueRef * MsgQueueRefList_findByName(MsgQueueRefList *listPtr, const char *name)
        ```
        finds a message queue reference that references a message queue with a given name. See [disastrOS_msg_queue.h](disastrOS_msg_queue.h) for more infos.
        
    -
        ```c
        void MsgQueueRefList_print(MsgQueueRefList *listPtr)
        ```
        prints the references list of message queues by a list of message queues references on the terminal. See [disastrOS_msg_queue.h](disastrOS_msg_queue.h) for more infos.
    
- **Modified functions**:

    -
        ```c
        void internal_openResource()
        ```
        added name handling. See [disastrOS_open_resource.c](disastrOS_open_resource.c) for more infos.
        
    -
        ```c
        void DescriptorList_print(ListHead *l)
        ```
        fixed printing bug. See [disastrOS_descriptor.c](disastrOS_descriptor.c) for more infos.
        
    -
        ```c
        void DescriptorPtrList_print(ListHead *l)
        ```
        fixed printing bug. See [disastrOS_descriptor.c](disastrOS_descriptor.c) for more infos.
        
- **New macros**:

    -
        ```c
        MAX_STRING_LEN
        ```
        is the max length of a string that can be allocated. See [disastrOS_constants.h](disastrOS_constants.h) for more infos.
        
    -
        ```c
        MAX_NUM_MESSAGES
        ```
        is the max number of messages that can be allocated. See [disastrOS_constants.h](disastrOS_constants.h) for more infos.
        
    -
        ```c
        MAX_NUM_MESSAGES_PER_MSG_QUEUE
        ```
        is the max number of messages that can be held by a message queue. See [disastrOS_constants.h](disastrOS_constants.h) for more infos.
    
    -
        ```c
        MAX_NUM_PRIORITIES
        ```
        is the max number of priority levels for messages in a message queue. See [disastrOS_constants.h](disastrOS_constants.h) for more infos.
    
    -
        ```c
        DSOS_EXECUTE
        ```
        is the flag for execute permission. See [disastrOS_constants.h](disastrOS_constants.h) for more infos.
        
    -
        ```c
        DSOS_CALL_MQ_CREATE
        ```
        is the system call number in the system call table of ```disastrOS_mqCreate()```. See [disastrOS_constants.h](disastrOS_constants.h) for more infos.
        
    -
        ```c
        DSOS_CALL_MQ_OPEN
        ```
        is the system call number in the system call table of ```disastrOS_mqOpen()```. See [disastrOS_constants.h](disastrOS_constants.h) for more infos.
        
    -
        ```c
        DSOS_CALL_MQ_CLOSE
        ```
        is the system call number in the system call table of ```disastrOS_mqClose()```. See [disastrOS_constants.h](disastrOS_constants.h) for more infos.
        
    -
        ```c
        DSOS_CALL_MQ_UNLINK
        ```
        is the system call number in the system call table of ```disastrOS_mqUnlink()```. See [disastrOS_constants.h](disastrOS_constants.h) for more infos.
        
    -
        ```c
        DSOS_CALL_MQ_READ
        ```
        is the system call number in the system call table of ```disastrOS_mqRead()```. See [disastrOS_constants.h](disastrOS_constants.h) for more infos.
        
    -
        ```c
        DSOS_CALL_MQ_WRITE
        ```
        is the system call number in the system call table of ```disastrOS_mqWrite()```. See [disastrOS_constants.h](disastrOS_constants.h) for more infos.

- **Modified macros**:

    -
        ```c
        DSOS_READ
        ```
        is the flag for read permission. See [disastrOS_constants.h](disastrOS_constants.h) for more infos.
        
    -
        ```c
        DSOS_WRITE
        ```
        is the flag for the write permission. See [disastrOS_constants.h](disastrOS_constants.h) for more infos.

### How to run
1. Modify [disastrOS_test.c](disastrOS_test.c) at your will or use the tests provided.
2. Open the root directory of the project in a terminal.
3. Set the project in order to be built with CMake. You can choose between two builds: the debug build (you can use 'gdb' and 'valgrind' at their best, but isn't optimized for better performance) and the release build (it is optimized for better performance, but you cab't use 'gdb' and 'valgrind' at their best).
Set the project build as 'Debug' with the first command line, as 'Release' with the second command line, instead.
```sh
$ cmake -DCMAKE_BUILD_TYPE=Debug .
$ cmake -DCMAKE_BUILD_TYPE=Release .
```
4. Do not modify the generated files.
5. Compile the project with
```sh
$ make
```
6. Run the project with the command below. Tips will be printed to help you choosing the test to run.
```sh
$ ./disastrOS_test [options]
```
7. You can get rid of all the compilation files (clean the build) but you will have to recompile (see 3., 4.) the project if you want to run it again.
Use this command line to clean the build
```sh
$ make clean
```

### Author
- Lorenzo Di Luccio (mat. 1797569): diluccio.1797569@studenti.uniroma1.it - lorenzo@diluccio.it
