#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <unistd.h>
#include <poll.h>
#include "disastrOS.h"

void sleeperFunc(void *argsPtr)
{
  char c;

  do
    c = getchar();
  while (c != 'q');
  disastrOS_exit(0);
}

void test0(void *argsPtr);
void test1(void *argsPtr);
void test2(void *argsPtr);
void test_mqCreate(void *argsPtr);
void test_mqOpen(void *argsPtr);
void test_mqClose(void *argsPtr);
void test_mqRead(void *argsPtr);
void test_mqWrite(void *argsPtr);
void test_mqUnlink(void *argsPtr);

void (*tests[])(void *) = { test0, test1, test2, test_mqCreate, test_mqOpen, test_mqClose,
  test_mqRead, test_mqWrite, test_mqUnlink };

void print_menu()
{
  printf("0:\tTest with single message queue and single thread.\n");
  printf("1:\tTest with single message queue and two \"synchronous\" threads, a reader and"
    " a writer.\n");
  printf("2:\tTest with single message queue and two \"asynchronous\" threads, a reader and"
  " a writer.\n");
  printf("3:\tTest the mqCreate() system call.\n");
  printf("4:\tTest the mqOpen() system call.\n");
  printf("5:\tTest the mqClose() system call.\n");
  printf("6:\tTest the mqRead() system call.\n");
  printf("7:\tTest the mqWrite() system call.\n");
  printf("8:\tTest the mqUnlink() system call.\n");
}

void print_usage()
{
  printf("Usage: ./disastrOS_test [test number, from 0 to 8]\n or \n");
  printf("Usage: ./disastrOS_test help for printing the menu\n");
}

int main(int argc, char** argv)
{
  setbuf(stdout, NULL);
  printf("Hello this is a disastrOS tester!\n");
  printf("TIP: When you see that, while a test is running, nothing happens for at least 10"
    " \"disastrOS seconds\", type 'q' and then press 'Enter' to terminate :) !\n");
  if (argc < 2)
  {
    print_usage();
    return -1;
  }
  if (strcmp(argv[1], "help") == 0)
  {
    print_menu();
    return 0;
  }

  unsigned choice = atoi(argv[1]);

  if (choice > 8)
  {
    printf("Number too high! Use ./disastrOS_test help for more infos.");
    return -2;
  }
  printf("Running test %u...\n", choice);
  srand(time(NULL));
  disastrOS_start(tests[choice], NULL, NULL);
  return 0;
}
