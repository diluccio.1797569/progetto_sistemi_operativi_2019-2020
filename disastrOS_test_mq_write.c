#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include "disastrOS.h"

void test_mqWrite_runnerFunc(void *argsPtr)
{
    int res;
    int exp;
    char buf[256];
    unsigned priority;
    unsigned passed_tests = 0;
    unsigned tests = 0;

    disastrOS_mqCreate("/mq");
    int mqfd = disastrOS_mqOpen("/mq", DSOS_READ | DSOS_WRITE);
    srand(time(NULL));
    disastrOS_printStatus();

    ++tests;
    exp = 9;
    printf("RUNNER mqWrite(mqfd, 'MESSAGGIO', 9, rand() %% 10)\n");
    res = disastrOS_mqWrite(mqfd, "MESSAGGIO", 9, rand() % 10);
    disastrOS_printStatus();
    passed_tests += (res == exp ? 1 : 0);
    printf("RUNNER Test %u: result = %d, expected = %d -> %s!\n\n", tests, res, exp,
        res == exp ? "PASSED" : "FAILED");
    
    ++tests;
    exp = 9;
    printf("RUNNER mqWrite(mqfd, 'OIGGASSEM', 9, rand() %% 10)\n");
    res = disastrOS_mqWrite(mqfd, "OIGGASSEM", 9, rand() % 10);
    disastrOS_printStatus();
    passed_tests += (res == exp ? 1 : 0);
    printf("RUNNER Test %u: result = %d, expected = %d -> %s!\n\n", tests, res, exp,
        res == exp ? "PASSED" : "FAILED");
    
    ++tests;
    exp = -1;
    printf("RUNNER mqWrite(-1, 'MSG', 3, rand() %% 10)\n");
    res = disastrOS_mqWrite(-1, "MSG", 3, rand() % 10);
    disastrOS_printStatus();
    passed_tests += (res == exp ? 1 : 0);
    printf("RUNNER Test %u: result = %d, expected = %d -> %s!\n\n", tests, res, exp,
        res == exp ? "PASSED" : "FAILED");

    ++tests;
    exp = -1;
    printf("RUNNER mqWrite(mqfd + 1, 'MSG', 3, rand() %% 10)\n");
    res = disastrOS_mqWrite(mqfd + 1, "MSG", 3, rand() % 10);
    disastrOS_printStatus();
    passed_tests += (res == exp ? 1 : 0);
    printf("RUNNER Test %u: result = %d, expected = %d -> %s!\n\n", tests, res, exp,
        res == exp ? "PASSED" : "FAILED");
    
    ++tests;
    exp = 0;
    printf("RUNNER mqWrite(mqfd, NULL, 3, rand() %% 10)\n");
    res = disastrOS_mqWrite(mqfd, NULL, 3, rand() % 10);
    disastrOS_printStatus();
    passed_tests += (res == exp ? 1 : 0);
    printf("RUNNER Test %u: result = %d, expected = %d -> %s!\n\n", tests, res, exp,
        res == exp ? "PASSED" : "FAILED");
    
    ++tests;
    exp = 0;
    printf("RUNNER mqWrite(mqfd, 'MSG', 0, rand() %% 10)\n");
    res = disastrOS_mqWrite(mqfd, "MSG", 0, rand() % 10);
    disastrOS_printStatus();
    passed_tests += (res == exp ? 1 : 0);
    printf("RUNNER Test %u: result = %d, expected = %d -> %s!\n\n", tests, res, exp,
        res == exp ? "PASSED" : "FAILED");

    ++tests;
    exp = -1;
    printf("RUNNER mqWrite(mqfd, 'MSG', 3, 10)\n");
    res = disastrOS_mqWrite(mqfd, "MSG", 3, 10);
    disastrOS_printStatus();
    passed_tests += (res == exp ? 1 : 0);
    printf("RUNNER Test %u: result = %d, expected = %d -> %s!\n\n", tests, res, exp,
        res == exp ? "PASSED" : "FAILED");
    
    res = 1;
    do
        res = disastrOS_mqWrite(mqfd, "MSG", 3, rand() % 10);
    while (res > 0);
    disastrOS_printStatus();

    ++tests;
    exp = 0;
    printf("RUNNER mqWrite(mqfd, 'MSG', 3, rand() %% 10)\n");
    res = disastrOS_mqWrite(mqfd, "MSG", 3, rand() % 10);
    disastrOS_printStatus();
    passed_tests += (res == exp ? 1 : 0);
    printf("RUNNER Test %u: result = %d, expected = %d -> %s!\n\n", tests, res, exp,
        res == exp ? "PASSED" : "FAILED");
    
    res = 1;
    do
        res = disastrOS_mqRead(mqfd, buf, 256, &priority);
    while (res > 0);

    ++tests;
    exp = -1;
    printf("RUNNER mqWrite(mqfd, 'MSG', 256, rand() %% 10)\n");
    res = disastrOS_mqWrite(mqfd, "MSG", 256, rand() % 10);
    disastrOS_printStatus();
    passed_tests += (res == exp ? 1 : 0);
    printf("RUNNER Test %u: result = %d, expected = %d -> %s!\n\n", tests, res, exp,
        res == exp ? "PASSED" : "FAILED");

    disastrOS_mqClose(mqfd);
    mqfd = disastrOS_mqOpen("/mq", DSOS_READ);
    disastrOS_printStatus();
    
    ++tests;
    exp = -1;
    printf("RUNNER mqWrite(mqfd, 'MSG', 3, rand() %% 10)\n");
    res = disastrOS_mqWrite(mqfd, "MSG", 3, rand() % 10);
    disastrOS_printStatus();
    passed_tests += (res == exp ? 1 : 0);
    printf("RUNNER Test %u: result = %d, expected = %d -> %s!\n\n", tests, res, exp,
        res == exp ? "PASSED" : "FAILED");

    disastrOS_mqClose(mqfd);
    disastrOS_mqUnlink("/mq");

    printf("\nRUNNER Total: %u/%u tests passed!\n\n", passed_tests, tests);

    printf("RUNNER exit(0)\n");
    disastrOS_exit(0);
}

void test_mqWrite(void *argsPtr)
{
    printf("INIT spawn(test_mqWrite_runnerFunc, NULL)\n");
    disastrOS_spawn(test_mqWrite_runnerFunc, NULL);
    disastrOS_printStatus();

    printf("INIT wait(0, NULL)\n");
    disastrOS_wait(0, NULL);
    disastrOS_printStatus();

    printf("INIT shutdown()\n");
    disastrOS_shutdown();
}