#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include "disastrOS.h"

void test0_runnerFunc(void *argsPtr)
{
    char buf[256];

    printf("RUNNER mqCreate('/mq')\n");
    disastrOS_mqCreate("/mq");
    disastrOS_printStatus();

    printf("RUNNER mqOpen('/mq', DSOS_READ | DSOS_WRITE)\n");
    int mqfd = disastrOS_mqOpen("/mq", DSOS_READ | DSOS_WRITE);
    disastrOS_printStatus();

    unsigned cycles = 2 + (rand() % 8);
    unsigned priority;

    for (unsigned i = 0; i < cycles; ++i)
    {
        memset(buf, 0, 256);
        priority = rand() % 10;
        sprintf(buf, "Msg %u.%u", i + 1, 1);
        printf("RUNNER buf: '%s', priority: %u\n\n", buf, priority);
        printf("RUNNER mqWrite(mqfd, buf, strlen(buf), priority)\n");
        disastrOS_mqWrite(mqfd, buf, strlen(buf), priority);
        disastrOS_printStatus();

        memset(buf, 0, 256);
        priority = rand() % 10;
        sprintf(buf, "Msg %u.%u", i + 1, 2);
        printf("RUNNER buf: '%s', priority: %u\n\n", buf, priority);
        printf("RUNNER mqWrite(mqfd, buf, strlen(buf), priority)\n");
        disastrOS_mqWrite(mqfd, buf, strlen(buf), priority);
        disastrOS_printStatus();

        memset(buf, 0, 256);
        printf("RUNNER mqRead(mqfd, buf, 256, &priority)\n");
        disastrOS_mqRead(mqfd, buf, 256, &priority);
        disastrOS_printStatus();
        printf("RUNNER buf: '%s', priority: %u\n\n", buf, priority);

        memset(buf, 0, 256);
        printf("RUNNER mqRead(mqfd, buf, 256, &priority)\n");
        disastrOS_mqRead(mqfd, buf, 256, &priority);
        disastrOS_printStatus();
        printf("RUNNER buf: '%s', priority: %u\n\n", buf, priority);
    }

    printf("RUNNER mqClose(mqfd)\n");
    disastrOS_mqClose(mqfd);
    disastrOS_printStatus();

    printf("RUNNER mqUnlink('/mq')\n");
    disastrOS_mqUnlink("/mq");
    disastrOS_printStatus();

    printf("RUNNER exit(0)\n");
    disastrOS_exit(0);
}

void test0(void *argsPtr)
{
    srand(time(NULL));

    printf("INIT spawn(test0_runnerFunc, NULL)\n");
    disastrOS_spawn(test0_runnerFunc, NULL);
    disastrOS_printStatus();

    printf("INIT wait(0, NULL)\n");
    disastrOS_wait(0, NULL);
    disastrOS_printStatus();

    printf("INIT shutdown()\n");
    disastrOS_shutdown();
}