#pragma once
#include "linked_list.h"
#include "disastrOS_pcb.h"
#include "disastrOS_resource.h"


struct DescriptorPtr;

typedef struct Descriptor{
  ListItem list;
  PCB* pcb;
  Resource* resource;
  int fd;
  unsigned read : 1; /* Read permission. It helps with the permissions handling in the
  message queues. */
  unsigned write : 1; /* Write permission. It helps with the permissions handling in the
  message queues. */
  unsigned execute : 1; /* Execute permission. It is ignored during the permissions handling
  in the message queues. */ 
  struct DescriptorPtr* ptr; // pointer to the entry in the resource list
} Descriptor;

typedef struct DescriptorPtr{
  ListItem list;
  Descriptor* descriptor;
} DescriptorPtr;

typedef ListHead DescriptorList;
typedef ListHead DescriptorPtrList;

void Descriptor_init();
Descriptor* Descriptor_alloc(int fd, Resource* res, PCB* pcb);
int Descriptor_free(Descriptor* d);
Descriptor*  DescriptorList_byFd(DescriptorList *l, int fd);
void DescriptorList_print(ListHead* l);

DescriptorPtr* DescriptorPtr_alloc(Descriptor* descriptor);
int DescriptorPtr_free(DescriptorPtr* d);
void DescriptorPtrList_print(ListHead* l);
DescriptorPtr * DescriptorPtrList_findByPCB(DescriptorPtrList *listPtr, PCB *pcbPtr);
