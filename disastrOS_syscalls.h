#pragma once
#include <assert.h>
#include "disastrOS.h"
#include "disastrOS_globals.h"

void internal_preempt();

void internal_fork();

void internal_exit();

void internal_wait();

void internal_spawn();

void internal_shutdown();

void internal_schedule();

void internal_sleep();

void internal_openResource();

void internal_closeResource();

void internal_destroyResource();

/**
 * @brief implements disastrOS_mqCreate(). @see disastrOS_mqCreate().
 */
void internal_mqCreate();

/**
 * @brief implements disastrOS_mqOpen(). @see disastrOS_mqOpen().
 */
void internal_mqOpen();

/**
 * @brief implements disastrOS_mqClose(). @see disastrOS_mqClose().
 */
void internal_mqClose();

/**
 * @brief implements disastrOS_mqUnlink(). @see disastrOS_mqUnlink().
 */
void internal_mqUnlink();

/**
 * @brief implements disastrOS_mqRead(). @see disastrOS_mqRead().
 */
void internal_mqRead();

/**
 * @brief implements disastrOS_mqWrite(). @see disastrOS_mqWrite().
 */
void internal_mqWrite();