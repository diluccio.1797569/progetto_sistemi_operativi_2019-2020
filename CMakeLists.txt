cmake_minimum_required(VERSION 3.10)
project(disastrOS)
set(CMAKE_C_STANDARD 99)

add_library(OSutils STATIC linked_list.h linked_list.c pool_allocator.h pool_allocator.c)

add_library(disastrOS STATIC disastrOS_constants.h disastrOS_pcb.h disastrOS_pcb.c
    disastrOS_globals.h disastrOS.h disastrOS.c disastrOS_syscalls.h disastrOS_fork.c
    disastrOS_exit.c disastrOS_wait.c disastrOS_schedule.c disastrOS_preempt.c
    disastrOS_spawn.c disastrOS_shutdown.c disastrOS_timer.h disastrOS_timer.c
    disastrOS_sleep.c disastrOS_descriptor.h disastrOS_descriptor.c disastrOS_resource.h disastrOS_resource.c disastrOS_open_resource.c
    disastrOS_close_resource.c disastrOS_destroy_resource.c disastrOS_msg_queue.h
    disastrOS_msg_queue.c disastrOS_mq_create.c disastrOS_mq_open.c disastrOS_mq_close.c
    disastrOS_mq_unlink.c disastrOS_mq_read.c disastrOS_mq_write.c)
target_link_libraries(disastrOS OSutils)

add_executable(linked_list_test linked_list_test.c)
target_link_libraries(linked_list_test OSutils)

add_executable(pool_allocator_test pool_allocator_test.c)
target_link_libraries(pool_allocator_test OSutils)

add_executable(disastrOS_test disastrOS_test0.c disastrOS_test1.c disastrOS_test2.c
    disastrOS_test_mq_create.c disastrOS_test_mq_open.c disastrOS_test_mq_close.c
    disastrOS_test_mq_unlink.c disastrOS_test_mq_read.c disastrOS_test_mq_write.c
    disastrOS_test.c)
target_link_libraries(disastrOS_test disastrOS)