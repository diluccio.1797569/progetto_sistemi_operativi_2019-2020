#include <string.h>
#include "disastrOS.h"
#include "disastrOS_syscalls.h"
#include "disastrOS_msg_queue.h"
#include "disastrOS_descriptor.h"

void internal_mqRead()
{
    int mqfd = (int) running->syscall_args[0];
    char *text = (char *) running->syscall_args[1];
    unsigned len = (unsigned) running->syscall_args[2];
    unsigned *priorityPtr = (unsigned *) running->syscall_args[3];

    if (mqfd < 0)
    {
        printf("A negative integer is passed as an argument!\n");
        running->syscall_retvalue = -1;
        return;
    }

    Descriptor *descPtr = DescriptorList_byFd(&(running->descriptors), mqfd);
    DescriptorPtr *descPtrPtr = descPtr != NULL ? descPtr->ptr : NULL;

    if (descPtr == NULL)
    {
        printf("The message queue whose related descriptor has fd %d isn't opened for the"
        " running process!\n", mqfd);
        running->syscall_retvalue = -1;
        return;
    }

    MsgQueue *msgQueuePtr = (MsgQueue *) descPtr->resource;

    if (descPtr->read == 0)
    {
        printf("The running process can't read from the message queue whose related"
            " descriptor has fd %d!\n It has %c%c%c permissions!\n", mqfd,
            descPtr->read == 1 ? 'r' : '-', descPtr->write == 1 ? 'w' : '-',
            descPtr->execute == 1 ? 'x' : '-');
        running->syscall_retvalue = -1;
        return;
    }
    if (text == NULL)
    {
        printf("The pointer to the char buffer is NULL!\n");
        running->syscall_retvalue = 0;
        return;
    }
    if (len == 0)
    {
        printf("The message length is equal to 0!\n");
        running->syscall_retvalue = 0;
        return;
    }
    
    MsgPrioritySubqueue *msgSubqueuePtr = NULL;

    for (unsigned priority = 0; priority < MAX_NUM_PRIORITIES; ++priority)
        if (msgQueuePtr->subqueuesPtrs[priority]->messages.size > 0)
        {
            msgSubqueuePtr = msgQueuePtr->subqueuesPtrs[priority];
            break;
        }
    if (msgSubqueuePtr == NULL)
    {
        printf("The message queue whose related descriptor has fd %d is empty!\n", mqfd);
        running->syscall_retvalue = 0;
        return;
    }

    Message *msgPtr = (Message *) msgSubqueuePtr->messages.first;

    if (len < msgPtr->len)
    {
        printf("The message length passed as an argument, %d, is less than %d, the actual"
            " one!\n", len, msgPtr->len);
        running->syscall_retvalue = -1;
        return;
    }
    strcpy(text, msgPtr->text);
    List_detach(&(msgSubqueuePtr->messages), (ListItem *) msgPtr);
    --(msgQueuePtr->numMessages);
    printf("Message read!\n");
    if (Message_delete(msgPtr) == -1)
    {
        printf("Failed to deallocate the message!\n");
        running->syscall_retvalue = -1;
        return;
    }
    if (priorityPtr != NULL)
        *priorityPtr = msgSubqueuePtr->priority;
    running->syscall_retvalue = msgPtr->len;
}