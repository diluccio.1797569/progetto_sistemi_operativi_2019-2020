#include <string.h>
#include "disastrOS.h"
#include "disastrOS_syscalls.h"
#include "disastrOS_msg_queue.h"
#include "disastrOS_descriptor.h"

void internal_mqWrite()
{
    int mqfd = (int) running->syscall_args[0];
    const char *text = (const char *) running->syscall_args[1];
    unsigned len = (unsigned) running->syscall_args[2];
    unsigned priority = (unsigned) running->syscall_args[3];

    if (mqfd < 0)
    {
        printf("A negative integer is passed as an argument!\n");
        running->syscall_retvalue = -1;
        return;
    }

    Descriptor *descPtr = DescriptorList_byFd(&(running->descriptors), mqfd);
    DescriptorPtr *descPtrPtr = descPtr != NULL ? descPtr->ptr : NULL;

    if (descPtr == NULL)
    {
        printf("The message queue whose related descriptor has fd %d isn't opened for the"
        " running process!\n", mqfd);
        running->syscall_retvalue = -1;
        return;
    }

    MsgQueue *msgQueuePtr = (MsgQueue *) descPtr->resource;

    if (descPtr->write == 0)
    {
        printf("The running process can't write in the message queue whose related"
            " descriptor has fd %d!\n It has %c%c%c permissions!\n", mqfd,
            descPtr->read == 1 ? 'r' : '-', descPtr->write == 1 ? 'w' : '-',
            descPtr->execute == 1 ? 'x' : '-');
        running->syscall_retvalue = -1;
        return;
    }
    if (text == NULL)
    {
        printf("The pointer to the char buffer is NULL!\n");
        running->syscall_retvalue = 0;
        return;
    }
    if (len == 0)
    {
        printf("The message length is equal to 0!\n");
        running->syscall_retvalue = 0;
        return;
    }
    if (priority >= MAX_NUM_PRIORITIES)
    {
        printf("The message priority is too high! It must be between 0 and %u!\n",
            MAX_NUM_PRIORITIES - 1);
        running->syscall_retvalue = -1;
        return;
    }
    if (msgQueuePtr->numMessages == MAX_NUM_MESSAGES_PER_MSG_QUEUE)
    {
        printf("The message queue whose related descriptor has fd %d is full!\n", mqfd);
        running->syscall_retvalue = 0;
        return;
    }
    if (len > MAX_STRING_LEN)
    {
        printf("The message length passed as an argument, %d, is greater than %d, the maximum"
            " string length!\n", len, MAX_STRING_LEN);
        running->syscall_retvalue = -1;
        return;
    }

    MsgPrioritySubqueue *msgSubqueuePtr = msgQueuePtr->subqueuesPtrs[priority];
    Message *newMsgPtr = Message_new(text, len);

    if (newMsgPtr == NULL)
    {
        printf("Failed to allocate the new message!\n");
        running->syscall_retvalue = -1;
        return;
    }
    List_insert(&(msgSubqueuePtr->messages), msgSubqueuePtr->messages.last,
        (ListItem *) newMsgPtr);
    ++(msgQueuePtr->numMessages);
    printf("Message written!\n");
    running->syscall_retvalue = len;
}