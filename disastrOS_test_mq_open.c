#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include "disastrOS.h"

void test_mqOpen_runnerFunc(void *argsPtr)
{
    int res;
    int exp;
    unsigned passed_tests = 0;
    unsigned tests = 0;

    disastrOS_mqCreate("/mq");
    disastrOS_printStatus();

    ++tests;
    exp = 0;
    printf("RUNNER mqOpen('/mq', DSOS_READ)\n");
    res = disastrOS_mqOpen("/mq", DSOS_READ);
    disastrOS_printStatus();
    passed_tests += (res == exp ? 1 : 0);
    printf("RUNNER Test %u: result = %d, expected = %d -> %s!\n\n", tests, res, exp,
        res == exp ? "PASSED" : "FAILED");
    
    disastrOS_mqClose(res);
    disastrOS_printStatus();

    ++tests;
    exp = 1;
    printf("RUNNER mqOpen('/mq', DSOS_WRITE)\n");
    res = disastrOS_mqOpen("/mq", DSOS_WRITE);
    disastrOS_printStatus();
    passed_tests += (res == exp ? 1 : 0);
    printf("RUNNER Test %u: result = %d, expected = %d -> %s!\n\n", tests, res, exp,
        res == exp ? "PASSED" : "FAILED");
    
    disastrOS_mqClose(res);
    disastrOS_printStatus();

    ++tests;
    exp = 2;
    printf("RUNNER mqOpen('/mq', DSOS_EXECUTE)\n");
    res = disastrOS_mqOpen("/mq", DSOS_EXECUTE);
    disastrOS_printStatus();
    passed_tests += (res == exp ? 1 : 0);
    printf("RUNNER Test %u: result = %d, expected = %d -> %s!\n\n", tests, res, exp,
        res == exp ? "PASSED" : "FAILED");
    
    disastrOS_mqClose(res);
    disastrOS_printStatus();

    ++tests;
    exp = 3;
    printf("RUNNER mqOpen('/mq', DSOS_READ | DSOS_WRITE)\n");
    res = disastrOS_mqOpen("/mq", DSOS_READ | DSOS_WRITE);
    disastrOS_printStatus();
    passed_tests += (res == exp ? 1 : 0);
    printf("RUNNER Test %u: result = %d, expected = %d -> %s!\n\n", tests, res, exp,
        res == exp ? "PASSED" : "FAILED");
    
    disastrOS_mqClose(res);
    disastrOS_printStatus();

    ++tests;
    exp = 4;
    printf("RUNNER mqOpen('/mq', DSOS_READ | DSOS_EXECUTE)\n");
    res = disastrOS_mqOpen("/mq", DSOS_READ | DSOS_EXECUTE);
    disastrOS_printStatus();
    passed_tests += (res == exp ? 1 : 0);
    printf("RUNNER Test %u: result = %d, expected = %d -> %s!\n\n", tests, res, exp,
        res == exp ? "PASSED" : "FAILED");
    
    disastrOS_mqClose(res);
    disastrOS_printStatus();

    ++tests;
    exp = 5;
    printf("RUNNER mqOpen('/mq', DSOS_WRITE | DSOS_EXECUTE)\n");
    res = disastrOS_mqOpen("/mq", DSOS_WRITE | DSOS_EXECUTE);
    disastrOS_printStatus();
    passed_tests += (res == exp ? 1 : 0);
    printf("RUNNER Test %u: result = %d, expected = %d -> %s!\n\n", tests, res, exp,
        res == exp ? "PASSED" : "FAILED");
    
    disastrOS_mqClose(res);
    disastrOS_printStatus();

    ++tests;
    exp = 6;
    printf("RUNNER mqOpen('/mq', DSOS_READ | DSOS_WRITE | DSOS_EXECUTE)\n");
    res = disastrOS_mqOpen("/mq", DSOS_READ | DSOS_WRITE | DSOS_EXECUTE);
    disastrOS_printStatus();
    passed_tests += (res == exp ? 1 : 0);
    printf("RUNNER Test %u: result = %d, expected = %d -> %s!\n\n", tests, res, exp,
        res == exp ? "PASSED" : "FAILED");
    
    disastrOS_mqClose(res);
    disastrOS_printStatus();

    ++tests;
    exp = -1;
    printf("RUNNER mqOpen(NULL, DSOS_READ | DSOS_WRITE)\n");
    res = disastrOS_mqOpen(NULL, DSOS_READ | DSOS_WRITE);
    disastrOS_printStatus();
    passed_tests += (res == exp ? 1 : 0);
    printf("RUNNER Test %u: result = %d, expected = %d -> %s!\n\n", tests, res, exp,
        res == exp ? "PASSED" : "FAILED");
    
    ++tests;
    exp = -1;
    printf("RUNNER mqOpen('/mq1', DSOS_READ | DSOS_WRITE)\n");
    res = disastrOS_mqOpen("/mq1", DSOS_READ | DSOS_WRITE);
    disastrOS_printStatus();
    passed_tests += (res == exp ? 1 : 0);
    printf("RUNNER Test %u: result = %d, expected = %d -> %s!\n\n", tests, res, exp,
        res == exp ? "PASSED" : "FAILED");
    
    int mqfd = disastrOS_mqOpen("/mq", DSOS_READ | DSOS_WRITE);

    ++tests;
    exp = 7;
    printf("RUNNER mqOpen('/mq', DSOS_READ | DSOS_WRITE)\n");
    res = disastrOS_mqOpen("/mq", DSOS_READ | DSOS_WRITE);
    disastrOS_printStatus();
    passed_tests += (res == exp ? 1 : 0);
    printf("RUNNER Test %u: result = %d, expected = %d -> %s!\n\n", tests, res, exp,
        res == exp ? "PASSED" : "FAILED");
    
    disastrOS_mqClose(mqfd);
    disastrOS_mqUnlink("/mq");
    disastrOS_printStatus();

    printf("\nRUNNER Total: %u/%u tests passed!\n\n", passed_tests, tests);

    printf("RUNNER exit(0)\n");
    disastrOS_exit(0);
}

void test_mqOpen(void *argsPtr)
{
    printf("INIT spawn(test_mqOpen_runnerFunc, NULL)\n");
    disastrOS_spawn(test_mqOpen_runnerFunc, NULL);
    disastrOS_printStatus();

    printf("INIT wait(0, NULL)\n");
    disastrOS_wait(0, NULL);
    disastrOS_printStatus();

    printf("INIT shutdown()\n");
    disastrOS_shutdown();
}