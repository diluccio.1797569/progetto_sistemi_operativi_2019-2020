#include "disastrOS_msg_queue.h"

#define STRING_SIZE sizeof((MAX_STRING_LEN + 1) * sizeof(char))
#define STRING_MEM_SIZE (STRING_SIZE + sizeof(int))
#define STRING_BUFFER_SIZE MAX_NUM_MESSAGES * STRING_MEM_SIZE
#define MESSAGE_SIZE sizeof(Message)
#define MESSAGE_MEM_SIZE (sizeof(Message) + sizeof(int))
#define MESSAGE_BUFFER_SIZE MAX_NUM_MESSAGES * MESSAGE_MEM_SIZE
#define MSG_PRIORITY_SUBQUEUE_SIZE sizeof(MsgPrioritySubqueue)
#define MSG_PRIORITY_SUBQUEUE_MEM_SIZE (sizeof(MsgPrioritySubqueue) + sizeof(int))
#define MSG_PRIORITY_SUBQUEUE_BUFFER_SIZE MAX_NUM_PRIORITIES * MAX_NUM_RESOURCES * MSG_PRIORITY_SUBQUEUE_MEM_SIZE
#define MSG_QUEUE_SIZE sizeof(MsgQueue)
#define MSG_QUEUE_MEM_SIZE (sizeof(MsgQueue) + sizeof(int))
#define MSG_QUEUE_BUFFER_SIZE MAX_NUM_RESOURCES * MSG_QUEUE_MEM_SIZE
#define MSG_QUEUE_REF_SIZE sizeof(MsgQueueRef)
#define MSG_QUEUE_REF_MEM_SIZE (sizeof(MsgQueueRef) + sizeof(int))
#define MSG_QUEUE_REF_BUFFER_SIZE MAX_NUM_RESOURCES * MSG_QUEUE_REF_MEM_SIZE

static char _strings_buffer[STRING_BUFFER_SIZE];
static PoolAllocator _strings_allocator;

static char _messages_buffer[MESSAGE_BUFFER_SIZE];
static PoolAllocator _messages_allocator;

static char _msg_priority_subqueues_buffer[MSG_PRIORITY_SUBQUEUE_BUFFER_SIZE];
static PoolAllocator _msg_priority_subqueues_allocator;

static char _msg_queues_buffer[MSG_QUEUE_BUFFER_SIZE];
static PoolAllocator _msg_queues_allocator;

static char _msg_queue_refs_buffer[MSG_QUEUE_REF_BUFFER_SIZE];
static PoolAllocator _msg_queue_refs_allocator;

void String_init()
{
    PoolAllocatorResult res = PoolAllocator_init(&_strings_allocator, STRING_SIZE,
        MAX_NUM_MESSAGES, _strings_buffer, STRING_BUFFER_SIZE);

    if (res != Success)
    {
        printf("Failed initialize the strings allocator!\n");
        assert(res == Success);
    }
}

char * String_new()
{
    char *newStr = (char *) PoolAllocator_getBlock(&_strings_allocator);

    if (newStr == NULL)
    {
        printf("Failed to allocate the new string!\n");
        return NULL;
    }
    memset(newStr, 0, STRING_MEM_SIZE);
    printf("String allocated!\n");
    return newStr;
}

int String_delete(char *str)
{
    if (str == NULL)
    {
        printf("A NULL pointer is passed as an argument!\n");
        return -1;
    }
    
    PoolAllocatorResult res = PoolAllocator_releaseBlock(&_strings_allocator, str);
    
    if (res != Success)
    {
        printf("Failed to deallocate the string !\n");
        return -1;
    }
    printf("String deallocated!\n");
    return 0;
}

void Message_init()
{
    PoolAllocatorResult res = PoolAllocator_init(&_messages_allocator, MESSAGE_SIZE,
        MAX_NUM_MESSAGES, _messages_buffer, MESSAGE_BUFFER_SIZE);

    if (res != Success)
    {
        printf("Failed initialize the messages allocator!\n");
        assert(res == Success);
    }
}

Message * Message_new(const char *text, unsigned len)
{
    char *newTextPtr = String_new();

    if (newTextPtr == NULL)
    {
        printf("Failed to allocate the new message!\n");
        return NULL;
    }

    Message *newMsgPtr = (Message *) PoolAllocator_getBlock(&_messages_allocator);

    if (newMsgPtr == NULL)
    {
        printf("Failed to allocate the new message!\n");
        String_delete(newTextPtr);
        return NULL;
    }
    newMsgPtr->_listItem_.prev = NULL;
    newMsgPtr->_listItem_.next = NULL;
    strcpy(newTextPtr, text);
    newMsgPtr->text = newTextPtr;
    newMsgPtr->len = len;
    printf("Message allocated!\n");
    return newMsgPtr;
}

int Message_delete(Message *msgPtr)
{
    if (msgPtr == NULL)
    {
        printf("A NULL pointer is passed as an argument!\n");
        return -1;
    }
    if (String_delete(msgPtr->text) == -1)
    {
        printf("Failed to deallocate the message!\n");
        return -1;
    }
    
    PoolAllocatorResult res = PoolAllocator_releaseBlock(&_messages_allocator, msgPtr);
    
    if (res != Success)
    {
        printf("Failed to deallocate the message!\n");
        return -1;
    }
    printf("Message deallocated!\n");
    return 0;
}

void Message_print(const Message *msgPtr)
{
    if (msgPtr == NULL)
        printf("[NULL]");
    else
        printf("\t\t(text: '%s', len: %d)", msgPtr->text, msgPtr->len);
}

void MessageList_print(MessageList *listPtr)
{
    printf("[\n");
    for (ListItem *currNodePtr = listPtr->first; currNodePtr != NULL;
        currNodePtr = currNodePtr->next)
    {
        Message *msgPtr = (Message *) currNodePtr;

        printf("\t");
        Message_print(msgPtr);
        if (currNodePtr->next != NULL)
            printf(",");
        printf("\n");
    }
    printf("\t\t]");
}

void MsgPrioritySubqueue_init()
{
    PoolAllocatorResult res = PoolAllocator_init(&_msg_priority_subqueues_allocator,
        MSG_PRIORITY_SUBQUEUE_SIZE, MAX_NUM_PRIORITIES * MAX_NUM_RESOURCES,
        _msg_priority_subqueues_buffer, MSG_PRIORITY_SUBQUEUE_BUFFER_SIZE);

    if (res != Success)
    {
        printf("Failed initialize the message subqueues allocator!\n");
        assert(res == Success);
    }
}

MsgPrioritySubqueue * MsgPrioritySubqueue_new(unsigned priority)
{
    MsgPrioritySubqueue *newMsgSubqueuePtr = (MsgPrioritySubqueue *)
        PoolAllocator_getBlock(&_msg_priority_subqueues_allocator);

    if (newMsgSubqueuePtr == NULL)
    {
        printf("Failed to allocate the new message subqueue!\n");
        return NULL;   
    }
    newMsgSubqueuePtr->_listItem_.prev = NULL;
    newMsgSubqueuePtr->_listItem_.next = NULL;
    List_init(&(newMsgSubqueuePtr->messages));
    newMsgSubqueuePtr->priority = priority;
    printf("Message subqueue allocated!\n");
    return newMsgSubqueuePtr;
}

int MsgPrioritySubqueue_delete(MsgPrioritySubqueue *msgSubqueuePtr)
{
    if (msgSubqueuePtr == NULL)
    {
        printf("A NULL pointer is passed as an argument!\n");
        return -1;
    }
    for (ListItem *currNodePtr = msgSubqueuePtr->messages.first; currNodePtr != NULL;)
    {
        Message *oldMsgPtr = (Message *) currNodePtr;

        currNodePtr = currNodePtr->next;
        List_detach(&(msgSubqueuePtr->messages), (ListItem *) oldMsgPtr);
        Message_delete(oldMsgPtr);
    }
    
    PoolAllocatorResult res = PoolAllocator_releaseBlock(&_msg_priority_subqueues_allocator,
        msgSubqueuePtr);
    
    if (res != Success)
    {
        printf("Failed to deallocate the message subqueue!\n");
        return -1;
    }
    printf("Message subqueue deallocated!\n");
    return 0;
}

void MsgPrioritySubqueue_print(const MsgPrioritySubqueue *msgSubqueuePtr)
{
    if (msgSubqueuePtr == NULL)
        printf("[NULL]");
    else
    {
        printf("\t\t[priority: %d, messages: ", msgSubqueuePtr->priority);
        MessageList_print((ListHead *) &(msgSubqueuePtr->messages));
        printf(", numMessages: %d]", msgSubqueuePtr->messages.size);
    } 
}

void MsgQueue_init()
{
    PoolAllocatorResult res = PoolAllocator_init(&_msg_queues_allocator, MSG_QUEUE_SIZE,
        MAX_NUM_RESOURCES, _msg_queues_buffer, MSG_QUEUE_BUFFER_SIZE);

    if (res != Success)
    {
        printf("Failed initialize the message queues allocator!\n");
        assert(res == Success);
    }
}

MsgQueue * MsgQueue_new(const char *name, int rid, PCB *ownerPCBPtr)
{
    MsgQueue *newMsgQueuePtr = (MsgQueue *) PoolAllocator_getBlock(&_msg_queues_allocator);

    if (newMsgQueuePtr == NULL)
    {
        printf("Failed to allocate the new message queue!\n");
        return NULL;   
    }
    newMsgQueuePtr->_resource_.list.prev = NULL;
    newMsgQueuePtr->_resource_.list.next = NULL;
    newMsgQueuePtr->_resource_.namePtr = name;
    newMsgQueuePtr->_resource_.rid = rid;
    newMsgQueuePtr->_resource_.type = DSOS_MSG_QUEUE_RESOURCE;
    List_init(&(newMsgQueuePtr->_resource_.descriptors_ptrs));
    for (unsigned priority = 0; priority < MAX_NUM_PRIORITIES; ++priority)
        newMsgQueuePtr->subqueuesPtrs[priority] = MsgPrioritySubqueue_new(priority);
    newMsgQueuePtr->ownerPCBPtr = ownerPCBPtr;
    newMsgQueuePtr->unlinkCalled = 0;
    newMsgQueuePtr->numMessages = 0;
    printf("Message queue allocated!\n");
    return newMsgQueuePtr;
}

int MsgQueue_delete(MsgQueue *msgQueuePtr)
{
    if (msgQueuePtr == NULL)
    {
        printf("A NULL pointer is passed as an argument!\n");
        return -1;
    }
    for (unsigned priority = 0; priority < MAX_NUM_PRIORITIES; ++priority)
        MsgPrioritySubqueue_delete(msgQueuePtr->subqueuesPtrs[priority]);
    
    PoolAllocatorResult res = PoolAllocator_releaseBlock(&_msg_queues_allocator,
        msgQueuePtr);
    
    if (res != Success)
    {
        printf("Failed to deallocate the message queue!\n");
        return -1;
    }
    printf("Message queue deallocated!\n");
    return 0;
}

void MsgQueue_print(const MsgQueue *msgQueuePtr)
{
    if (msgQueuePtr == NULL)
        printf("[NULL]");
    else
    {
        printf("[name: '%s', rid: %d, descs: ", msgQueuePtr->_resource_.namePtr,
            msgQueuePtr->_resource_.rid);
        DescriptorPtrList_print((ListHead *) &(msgQueuePtr->_resource_.descriptors_ptrs));
        printf(", msgSubqueues:\n");
        for (unsigned priority = 0; priority < MAX_NUM_PRIORITIES; ++priority)
        {
            MsgPrioritySubqueue_print(msgQueuePtr->subqueuesPtrs[priority]);
            if (priority + 1 < MAX_NUM_PRIORITIES)
                printf(",\n");
        }
        printf("\n\t]");
    } 
}

void MsgQueueRef_init()
{
    PoolAllocatorResult res = PoolAllocator_init(&_msg_queue_refs_allocator,
        MSG_QUEUE_REF_SIZE, MAX_NUM_RESOURCES, _msg_queue_refs_buffer,
        MSG_QUEUE_REF_BUFFER_SIZE);

    if (res != Success)
    {
        printf("Failed initialize the message queue references allocator!\n");
        assert(res == Success);
    }
}

MsgQueueRef * MsgQueueRef_new(MsgQueue *msgQueuePtr)
{
    MsgQueueRef *newMsgQueueRefPtr = (MsgQueueRef *)
        PoolAllocator_getBlock(&_msg_queue_refs_allocator);

    if (newMsgQueueRefPtr == NULL)
    {
        printf("Failed to allocate the new message queue reference!\n");
        return NULL;   
    }
    newMsgQueueRefPtr->msgQueuePtr = msgQueuePtr;
    printf("Message queue reference for the new message queue allocated!\n");
    return newMsgQueueRefPtr;
}

int MsgQueueRef_delete(MsgQueueRef *msgQueueRefPtr)
{
    if (msgQueueRefPtr == NULL)
    {
        printf("A NULL pointer is passed as an argument!\n");
        return -1;
    }
    
    PoolAllocatorResult res = PoolAllocator_releaseBlock(&_msg_queue_refs_allocator,
        msgQueueRefPtr);
    
    if (res != Success)
    {
        printf("Failed to deallocate the message queue reference!\n");
        printf("!\n");
        return -1;
    }
    printf("Message queue reference for the message queue deallocated!\n");
    return 0;
}

void MsgQueueRef_print(const MsgQueueRef *msgQueueRefPtr)
{
    MsgQueue_print(msgQueueRefPtr->msgQueuePtr);
}

MsgQueueRef * MsgQueueRefList_findByName(MsgQueueRefList *listPtr, const char *name)
{
    if (name == NULL)
        return NULL;
    
    for (ListItem *currNodePtr = listPtr->first; currNodePtr != NULL;
        currNodePtr = currNodePtr->next)
        if (strcmp(name, ((MsgQueueRef *) currNodePtr)->msgQueuePtr->_resource_.namePtr) == 0)
            return (MsgQueueRef *) currNodePtr;
    return NULL;
}

void MsgQueueRefList_print(MsgQueueRefList *listPtr)
{
    printf("{\n");
    for (ListItem *currNodePtr = listPtr->first; currNodePtr != NULL;
        currNodePtr = currNodePtr->next)
    {
        MsgQueueRef *msgQueueRefPtr = (MsgQueueRef *) currNodePtr;

        printf("\t");
        MsgQueueRef_print(msgQueueRefPtr);
        if (currNodePtr->next != NULL)
            printf(",");
        printf("\n");
    }
    printf("}\n");
}