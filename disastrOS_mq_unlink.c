#include "disastrOS.h"
#include "disastrOS_syscalls.h"
#include "disastrOS_msg_queue.h"
#include "disastrOS_descriptor.h"

void internal_mqUnlink()
{
    const char *name = (const char *) running->syscall_args[0];

    if (name == NULL)
    {
        printf("A NULL pointer is passed as an argument!\n");
        running->syscall_retvalue = -1;
        return;
    }

    MsgQueueRef *msgQueueRefPtr = MsgQueueRefList_findByName(&msg_queues_list, name);
    MsgQueue *msgQueuePtr = msgQueueRefPtr != NULL ? msgQueueRefPtr->msgQueuePtr : NULL;

    if (msgQueuePtr == NULL)
    {
        printf("A message queue with name '%s' doesn't exist!\n", name);
        running->syscall_retvalue = -1;
        return;
    }
    if (running != msgQueuePtr->ownerPCBPtr)
    {
        printf("The running process doesn't own the message queue with name '%s'!\n", name);
        running->syscall_retvalue = -1;
        return;
    }
    msgQueuePtr->unlinkCalled = 1;
    if (msgQueuePtr->_resource_.descriptors_ptrs.size > 0)
    {
        printf("The message queue with name '%s' has opened descriptors!\n", name);
        running->syscall_retvalue = 0;
        return;
    }
    List_detach(&msg_queues_list, (ListItem *) msgQueueRefPtr);
    List_detach(&resources_list, (ListItem *) msgQueuePtr);
    if (MsgQueueRef_delete(msgQueueRefPtr) != 0)
    {
        printf("Failed to deallocate the message queue reference for the message queue with"
            " name '%s'\n!", name);
        running->syscall_retvalue = -1;
        return;
    }
    if (MsgQueue_delete(msgQueuePtr) != 0)
    {
        printf("Failed to deallocate the message queue with name '%s'!\n", name);
        running->syscall_retvalue = -1;
        return;
    }
    printf("Message queue with name '%s' destroyed!\n", name);
    running->syscall_retvalue = 0;
}
