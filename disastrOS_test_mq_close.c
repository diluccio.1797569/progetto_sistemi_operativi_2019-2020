#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include "disastrOS.h"

void test_mqClose_runnerFunc(void *argsPtr)
{
    int res;
    int exp;
    unsigned passed_tests = 0;
    unsigned tests = 0;

    disastrOS_mqCreate("/mq");
    int mqfd = disastrOS_mqOpen("/mq", DSOS_READ | DSOS_WRITE);
    disastrOS_printStatus();

    ++tests;
    exp = 0;
    printf("RUNNER mqClose(mqfd)\n");
    res = disastrOS_mqClose(mqfd);
    disastrOS_printStatus();
    passed_tests += (res == exp ? 1 : 0);
    printf("RUNNER Test %u: result = %d, expected = %d -> %s!\n\n", tests, res, exp,
        res == exp ? "PASSED" : "FAILED");
    
    mqfd = disastrOS_mqOpen("/mq", DSOS_READ | DSOS_WRITE);
    disastrOS_printStatus();

    ++tests;
    exp = -1;
    printf("RUNNER mqClose(-1)\n");
    res = disastrOS_mqClose(-1);
    disastrOS_printStatus();
    passed_tests += (res == exp ? 1 : 0);
    printf("RUNNER Test %u: result = %d, expected = %d -> %s!\n\n", tests, res, exp,
        res == exp ? "PASSED" : "FAILED");
    
    disastrOS_mqClose(mqfd);
    disastrOS_printStatus();

    ++tests;
    exp = 0;
    printf("RUNNER mqClose(mqfd)\n");
    res = disastrOS_mqClose(mqfd);
    disastrOS_printStatus();
    passed_tests += (res == exp ? 1 : 0);
    printf("RUNNER Test %u: result = %d, expected = %d -> %s!\n\n", tests, res, exp,
        res == exp ? "PASSED" : "FAILED");
    
    mqfd = disastrOS_mqOpen("/mq", DSOS_READ | DSOS_WRITE);
    disastrOS_mqUnlink("/mq");
    disastrOS_printStatus();

    ++tests;
    exp = 0;
    printf("RUNNER mqClose(mqfd)\n");
    res = disastrOS_mqClose(mqfd);
    disastrOS_printStatus();
    passed_tests += (res == exp ? 1 : 0);
    printf("RUNNER Test %u: result = %d, expected = %d -> %s!\n\n", tests, res, exp,
        res == exp ? "PASSED" : "FAILED");

    printf("\nRUNNER Total: %u/%u tests passed!\n\n", passed_tests, tests);

    printf("RUNNER exit(0)\n");
    disastrOS_exit(0);
}

void test_mqClose(void *argsPtr)
{
    printf("INIT spawn(test_mqClose_runnerFunc, NULL)\n");
    disastrOS_spawn(test_mqClose_runnerFunc, NULL);
    disastrOS_printStatus();

    printf("INIT wait(0, NULL)\n");
    disastrOS_wait(0, NULL);
    disastrOS_printStatus();

    printf("INIT shutdown()\n");
    disastrOS_shutdown();
}