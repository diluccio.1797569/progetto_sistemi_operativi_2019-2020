#include "disastrOS.h"
#include "disastrOS_syscalls.h"
#include "disastrOS_msg_queue.h"
#include "disastrOS_descriptor.h"

void internal_mqOpen()
{
    const char *name = (const char *) running->syscall_args[0];
    int flags = (int) running->syscall_args[1];

    if (name == NULL)
    {
        printf("A NULL pointer is passed as an argument!\n");
        running->syscall_retvalue = -1;
        return;
    }

    MsgQueueRef *msgQueueRefPtr = MsgQueueRefList_findByName(&msg_queues_list, name);
    MsgQueue *msgQueuePtr = msgQueueRefPtr != NULL ? msgQueueRefPtr->msgQueuePtr : NULL;
    
    if (msgQueuePtr == NULL)
    {
        printf("A message queue with name '%s' doesn't exist!\n", name);
        running->syscall_retvalue = -1;
        return;
    }

    DescriptorPtr *existingDescPtrPtr = DescriptorPtrList_findByPCB(
        &(msgQueuePtr->_resource_.descriptors_ptrs), running);
    Descriptor *existingDescPtr = existingDescPtrPtr != NULL ? existingDescPtrPtr->descriptor
        : NULL;
    
    if (existingDescPtr != NULL)
    {
        printf("The message queue with name '%s' is already opened for the running"
        " process!\n It has fd %d\n", name, existingDescPtr->fd);
        running->syscall_retvalue = existingDescPtr->fd;
        return;
    }

    Descriptor *newDescPtr = Descriptor_alloc(running->last_fd++, (Resource *) msgQueuePtr,
        running);
    
    if (newDescPtr == NULL)
    {
        printf("Failed to allocate the new descriptor for the message queue with name"
        " '%s'!\n", name);
        running->syscall_retvalue = -1;
        return;
    }
    newDescPtr->read = (flags & DSOS_READ) != 0 ? 1 : 0;
    newDescPtr->write = (flags & DSOS_WRITE) != 0 ? 1 : 0;
    newDescPtr->execute = 0;

    DescriptorPtr *newDescPtrPtr = DescriptorPtr_alloc(newDescPtr);

    if (newDescPtrPtr == NULL)
    {
        printf("Failed to allocate the new descriptor reference for the descriptor with fd"
            " %d", newDescPtr->fd);
    }
    newDescPtr->ptr = newDescPtrPtr;
    List_insert(&(running->descriptors), running->descriptors.last, (ListItem *) newDescPtr);
    List_insert(&(msgQueuePtr->_resource_.descriptors_ptrs),
        msgQueuePtr->_resource_.descriptors_ptrs.last, (ListItem *) newDescPtrPtr);
    printf("Message queue with name '%s' opened with permissions %c%c%c!\n", name,
        newDescPtr->read == 0 ? '-' : 'r',
        newDescPtr->write == 0 ? '-' : 'w',
        newDescPtr->execute == 0 ? '-' : 'x');
    running->syscall_retvalue = newDescPtr->fd;
}