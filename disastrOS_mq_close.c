#include "disastrOS.h"
#include "disastrOS_syscalls.h"
#include "disastrOS_msg_queue.h"
#include "disastrOS_descriptor.h"

void internal_mqClose()
{
    int mqfd = (int) running->syscall_args[0];

    if (mqfd < 0)
    {
        printf("A negative integer is passed as an argument!\n");
        running->syscall_retvalue = -1;
        return;
    }

    Descriptor *descPtr = DescriptorList_byFd(&(running->descriptors), mqfd);
    DescriptorPtr *descPtrPtr = descPtr != NULL ? descPtr->ptr : NULL;

    if (descPtr == NULL)
    {
        printf("The message queue whose related descriptor has fd %d is already closed for"
        " the running process!\n", mqfd);
        running->syscall_retvalue = 0;
        return;
    }

    MsgQueue *msgQueuePtr = (MsgQueue *) descPtr->resource;

    List_detach(&(msgQueuePtr->_resource_.descriptors_ptrs), (ListItem *) descPtrPtr);
    List_detach(&(running->descriptors), (ListItem *) descPtr);
    if (DescriptorPtr_free(descPtrPtr) != 0)
    {
        printf("Failed to deallocate the descriptor reference for the descriptor with fd"
            "%d!\n", mqfd);
        running->syscall_retvalue = -1;
        return;
    }
    if (Descriptor_free(descPtr) != 0)
    {
        printf("Failed to deallocate the descriptor with fd %d!\n", mqfd);
        running->syscall_retvalue = -1;
        return;
    }
    printf("Message queue whose related descriptor has fd %d closed!\n", mqfd);
    if (msgQueuePtr->_resource_.descriptors_ptrs.size == 0 && msgQueuePtr->unlinkCalled)
    {
        printf("The message queue whose related descriptor has fd %d has no related"
            " descriptors and disastrOS_mqUnlink() was called!\n", mqfd);

        running->syscall_args[0] = (long) msgQueuePtr->_resource_.namePtr;
        internal_mqUnlink();
        if (running->syscall_retvalue == -1)
            return;
    }
    running->syscall_retvalue = 0;
}