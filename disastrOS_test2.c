#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include "disastrOS.h"

void test2_writerFunc(void *argsPtr)
{
    char buf[256];

    disastrOS_printStatus();

    printf("WRITER mqOpen('/mq', DSOS_WRITE)\n");
    int mqfd = disastrOS_mqOpen("/mq", DSOS_WRITE);
    disastrOS_printStatus();

    printf("WRITER preempt()\n");
    disastrOS_preempt();
    disastrOS_printStatus();

    unsigned cycles = 2 + (rand() % 16);
    unsigned priority;

    for (unsigned i = 0; i < cycles; ++i)
    {
        memset(buf, 0, 256);
        priority = rand() % 10;
        sprintf(buf, "Msg %u.%u", i + 1, 1);
        printf("WRITER buf: '%s', priority: %u\n\n", buf, priority);
        printf("WRITER mqWrite(mqfd, buf, strlen(buf), priority)\n");
        disastrOS_mqWrite(mqfd, buf, strlen(buf), priority);
        disastrOS_printStatus();

        printf("WRITER sleep(rand() %% 8)\n");
        disastrOS_sleep(rand() % 8);

        memset(buf, 0, 256);
        priority = rand() % 10;
        sprintf(buf, "Msg %u.%u", i + 1, 2);
        printf("WRITER buf: '%s', priority: %u\n\n", buf, priority);
        printf("WRITER mqWrite(mqfd, buf, strlen(buf), priority)\n");
        disastrOS_mqWrite(mqfd, buf, strlen(buf), priority);
        disastrOS_printStatus();

        printf("WRITER sleep(rand() %% 8)\n");
        disastrOS_sleep(rand() % 8);
    }

    printf("WRITER mqClose(mqfd)\n");
    disastrOS_mqClose(mqfd);
    disastrOS_printStatus();

    printf("WRITER preempt()\n");
    disastrOS_preempt();
    disastrOS_printStatus();

    printf("WRITER exit(0)\n");
    disastrOS_exit(0);
}

void test2_readerFunc(void *argsPtr)
{
    char buf[256];

    disastrOS_printStatus();

    printf("READER mqOpen('/mq', DSOS_READ)\n");
    int mqfd = disastrOS_mqOpen("/mq", DSOS_READ);
    disastrOS_printStatus();

    printf("READER preempt()\n");
    disastrOS_preempt();
    disastrOS_printStatus();

    unsigned cycles = 2 + (rand() % 16);
    unsigned priority = 20;

    for (unsigned i = 0; i < cycles; ++i)
    {
        memset(buf, 0, 256);
        printf("READER mqRead(mqfd, buf, 256, &priority)\n");
        disastrOS_mqRead(mqfd, buf, 256, &priority);
        disastrOS_printStatus();
        printf("READER buf: '%s', priority: %u\n\n", buf, priority);

        printf("READER sleep(rand() %% 8)\n");
        disastrOS_sleep(rand() % 8);

        memset(buf, 0, 256);
        printf("READER mqRead(mqfd, buf, 256, &priority)\n");
        disastrOS_mqRead(mqfd, buf, 256, &priority);
        disastrOS_printStatus();
        printf("READER buf: '%s', priority: %u\n\n", buf, priority);

        printf("READER sleep(rand() %% 8)\n");
        disastrOS_sleep(rand() % 8);
    }

    printf("READER mqClose(mqfd)\n");
    disastrOS_mqClose(mqfd);
    disastrOS_printStatus();

    printf("READER preempt()\n");
    disastrOS_preempt();
    disastrOS_printStatus();

    printf("READER exit(0)\n");
    disastrOS_exit(0);
}

void test2_runnerFunc(void *argsPtr)
{
    printf("RUNNER spawn(test2_writerFunc, NULL)\n");
    disastrOS_spawn(test2_readerFunc, NULL);
    disastrOS_printStatus();

    printf("RUNNER spawn(test2_readerFunc, NULL)\n");
    disastrOS_spawn(test2_writerFunc, NULL);
    disastrOS_printStatus();

    printf("RUNNER mqCreate('/mq')\n");
    disastrOS_mqCreate("/mq");
    disastrOS_printStatus();

    char c;

    do
        c = getchar();
    while (c != 'q');

    printf("RUNNER wait(0, NULL)\n");
    disastrOS_wait(0, NULL);
    disastrOS_printStatus();

    printf("RUNNER wait(0, NULL)\n");
    disastrOS_wait(0, NULL);
    disastrOS_printStatus();

    printf("RUNNER mqUnlink('/mq')\n");
    disastrOS_mqUnlink("/mq");
    disastrOS_printStatus();

    printf("RUNNER exit(0)\n");
    disastrOS_exit(0);
}

void test2(void *argsPtr)
{
    srand(time(NULL));

    printf("INIT spawn(test2_runnerFunc, NULL)\n");
    disastrOS_spawn(test2_runnerFunc, NULL);
    disastrOS_printStatus();

    printf("INIT wait(0, NULL)\n");
    disastrOS_wait(0, NULL);
    disastrOS_printStatus();

    printf("INIT shutdown()\n");
    disastrOS_shutdown();
}