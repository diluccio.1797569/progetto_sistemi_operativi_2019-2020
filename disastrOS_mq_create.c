#include "disastrOS.h"
#include "disastrOS_syscalls.h"
#include "disastrOS_msg_queue.h"
#include "disastrOS_descriptor.h"

void internal_mqCreate()
{
    const char *name = (const char *) running->syscall_args[0];

    if (name == NULL)
    {
        printf("A NULL pointer is passed as an argument!\n");
        running->syscall_retvalue = -1;
        return;
    }

    MsgQueueRef *existingMsgQueueRefPtr = MsgQueueRefList_findByName(&msg_queues_list, name);
    MsgQueue *existingMsgQueuePtr = existingMsgQueueRefPtr != NULL ?
        existingMsgQueueRefPtr->msgQueuePtr : NULL;

    if (existingMsgQueuePtr != NULL)
    {
        printf("A message queue with name '%s' already exists!\n", name);
        running->syscall_retvalue = 0;
        return;
    }

    int rid = last_rid++;

    if (rid >= MAX_NUM_RESOURCES)
    {
        printf("Too many message queues alredy exist!\n");
        running->syscall_retvalue = -1;
        return;
    }

    MsgQueue *newMsgQueuePtr = MsgQueue_new(name, rid, running);

    if (newMsgQueuePtr == NULL)
    {
        printf("Failed to allocate the new message queue with name '%s'!\n", name);
        running->syscall_retvalue = -1;
        return;
    }

    MsgQueueRef *newMsgQueueRefPtr = MsgQueueRef_new(newMsgQueuePtr);

    if (newMsgQueueRefPtr == NULL)
    {
        printf("Failed to allocate the new message queue reference for the message queue with"
            " name '%s'!\n", name);
        running->syscall_retvalue = -1;
        return;
    }

    List_insert(&resources_list, resources_list.last, (ListItem *) newMsgQueuePtr);
    List_insert(&msg_queues_list, msg_queues_list.last, (ListItem *) newMsgQueueRefPtr);
    printf("Message queue with name '%s' created!\n", name);
    running->syscall_retvalue = 0;
}