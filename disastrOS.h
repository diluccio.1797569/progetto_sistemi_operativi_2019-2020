#pragma once
#include "disastrOS_pcb.h"
#include "disastrOS_msg_queue.h"
#include "linked_list.h"

#ifdef _DISASTROS_DEBUG_
#include <stdio.h>

#define disastrOS_debug(...) printf(__VA_ARGS__) 

#else //_DISASTROS_DEBUG_

#define disastrOS_debug(...) ;

#endif //_DISASTROS_DEBUG_

// initializes the structures and spawns a fake init process
void disastrOS_start(void (*f)(void*), void* args, char* logfile);

// generic syscall 
int disastrOS_syscall(int syscall_num, ...);

// classical process control
int disastrOS_getpid(); // this should be a syscall, but we have no memory separation, so we return just the running pid
int disastrOS_fork();
void disastrOS_exit(int exit_value);
int disastrOS_wait(int pid, int* retval);
void disastrOS_preempt();
void disastrOS_spawn(void (*f)(void*), void* args );
void disastrOS_shutdown();

// timers
void disastrOS_sleep(int);

// resources (files)
int disastrOS_openResource(const char *namePtr, int resource_id, int type, int mode);
int disastrOS_closeResource(int fd);
int disastrOS_destroyResource(int resource_id);

//message queues
//TODO add a priority

/**
 * @brief Allocates and creates a message queue with a given name that will be owned by the
 * running process.
 * If a message queue with such name already exists, this function simply returns and does
 * nothing.
 * @param name pointer to a string that is the name of the message queue to create.
 * @returns 0 or -1 if an error occours. 
 */
int disastrOS_mqCreate(const char *name);

/**
 * @brief Opens a message queue with a given name.
 * The message queue MUST be created with disastrOS_mqCreate().
 * If the message queue with such name is already opened, this function simply returns the
 * related file descriptor and does nothing.
 * @param name pointer to a string that is the name of the message queue to open.
 * @param flags integer that holds the way the message queue must be opened. Use 0 for
 * nothing, DSOS_READ for read-only, DSOS_WRITE for write-only, DSOS_READ | DSOS_WRITE for
 * read-and-write. DSOS_EXECUTE is ignored, instead. If other values are passed, they can
 * cause an undefined behaviour of this function.
 * @returns a non-negative integer that is the file descriptor for the opened message queue or
 * -1 if an error occours. 
 */
int disastrOS_mqOpen(const char *name, int flags);

/**
 * @brief Closes a message queue whose related descriptor is identified by a given file
 * descriptor.
 * The message queue MUST be opened with disastrOS_mqOpen().
 * If the message queue with such related descriptor is already closed, this function simply
 * returns and does nothing.
 * If the message queue with such related descriptor has no more related descriptors after
 * this function returns and disastrOS_mqUnlink() marks it, it will be destroyed and
 * deallocated.
 * @param mqfd a non-negative integer that is the file descriptor for the message queue to
 * close.
 * @returns 0 or -1 if and error occours.
 */
int disastrOS_mqClose(int mqfd);

/**
 * @brief Destroys and deallocates a message queue with a given name owned by the running
 * process.
 * The message queue MUST be created with disastrOS_mqCreate().
 * If the message queue with such name isn't owned by the running process, this function
 * simply returns with an error and does nothing.
 * If the message queue with such name has one or more related descriptors, this function
 * simply marks it, returns and does nothing, but when the last of these descriptors will be
 * deallocated, the message queue will be finally destroyed and deallocated.
 * @param name pointer to a string that is the name of the message queue to destroy.
 * @returns 0 or -1 if an error occours.
 */
int disastrOS_mqUnlink(const char *name);

/**
 * @brief Removes the message with higher priority from the message queue whose related
 * descriptor is identified by a given file descriptor and writes it in a given char buffer
 * with a specified length.
 * The message queue MUST be opened with disastrOS_mqOpen() and the buffer MUST have a length
 * greater or equal to the message length.
 * If the char buffer pointer is NULL, or if the message length is equal to 0, or if the
 * message queue is empty, this function simply returns 0 and does nothing.
 * @param mqfd a non-negative integer that represents the file descriptor for the message
 * queue where to read from.
 * @param text pointer to a string that is the char buffer.
 * @param len length of the buffer.
 * @param priorityPtr pointer to an integer where the message priority is written. If this
 * information isn't requested, NULL can be passed as argument.
 * @returns the length of the read message or -1 if an error occours.
 */
int disastrOS_mqRead(int mqfd, char *text, unsigned len, unsigned *priorityPtr);

/**
 * @brief Reads a message from a given char buffer with a specified length and adds it, with
 * an associated priority, in the the message queue whose related descriptor is identified by
 * a given file descriptor.
 * The message queue MUST be opened with disastrOS_mqOpen(), the buffer MUST have a length
 * less or equal to the maximum string length, which is 255 and the priority MUST be between
 * 0 and MAX_NUM_PRIORITIES - 1.
 * If the char buffer pointer is NULL, of if the message length is equal to 0, or if the
 * message queue is full, this function simply returns 0 and does nothing.
 * @param mqfd a non-negative integer that represents the file descriptor for the message
 * queue where to write in.
 * @param text pointer to a string that is the char buffer.
 * @param len length of the message.
 * @param priority priority to associate to the message.
 * @return the length of the written message or -1 if an error occours.
 */
int disastrOS_mqWrite(int mqfd, const char *text, unsigned len, unsigned priority);

// debug function, prints the state of the internal system
void disastrOS_printStatus();
